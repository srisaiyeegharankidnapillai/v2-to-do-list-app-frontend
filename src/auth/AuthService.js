import auth0 from 'auth0-js'
import { AUTH_CONFIG } from './auth0-variables'
import EventEmitter from 'eventemitter3'
import router from './../router'
import {loginCall} from "../api"
import axios from 'axios';


export default class AuthService {

  authenticated = this.isAuthenticated()
  authNotifier = new EventEmitter()


  constructor () {
    this.login = this.login.bind(this)
    this.setSession = this.setSession.bind(this)
    this.logout = this.logout.bind(this)
    this.isAuthenticated = this.isAuthenticated.bind(this)
    //this.checkUserApplication = this.checkUserApplication.bind(this)
  }

  auth0 = new auth0.WebAuth({
    domain: AUTH_CONFIG.domain,
    clientID: AUTH_CONFIG.clientId,
    redirectUri: AUTH_CONFIG.callbackUrl,
    audience: `https://${AUTH_CONFIG.domain}/userinfo`,
    responseType: 'token id_token',
    scope: 'openid profile email'
  })

  // get profile() {
  //   this.auth0.client.userInfo( authResult.accessToken, function(err, profile) {
  //       console.log("user is");
  //       return profile;
  //   });
  // }

  login () {
    this.auth0.authorize()
  }

  getUserInfo(authResult) 
  {
    this.auth0.client.userInfo( authResult.accessToken, function(err, userProfile) {
        console.log("user is");
        console.log(userProfile);

        var url = loginCall();
        var reqBody = {
          userEmail : userProfile.email,
          userFirstName : userProfile.given_name,
          userLastName : userProfile.family_name,
          userPassword : ""
        }

        var self = this;
        axios.post(url, reqBody)
        .then(function(response){
          console.log("Message from login");
          localStorage.setItem('user_id', response.data.userId); 
          localStorage.setItem('user_name', response.data.userFirstName)
          console.log(response.data)
          router.replace('home')

        })
        .catch(function(error){
          console.log(error);
        }) 



      });
        

  }  

  handleAuthentication () {
    this.auth0.parseHash((err, authResult) => {
      if (authResult && authResult.accessToken && authResult.idToken) {
        this.setSession(authResult);
        this.getUserInfo(authResult);
        
      } else if (err) {
        router.replace('home')
        console.log(err)
        alert(`Error: ${err.error}. Check the console for further details.`)
      }
    })
  }

  setSession (authResult) {
    // Set the time that the access token will expire at
    let expiresAt = JSON.stringify(
      authResult.expiresIn * 1000 + new Date().getTime()
    )
    console.log("ID token is");
    console.log(authResult.idToken);
    localStorage.setItem('access_token', authResult.accessToken)
    localStorage.setItem('id_token', authResult.idToken)
    localStorage.setItem('expires_at', expiresAt)
    this.authNotifier.emit('authChange', { authenticated: true })

  }

  logout () {
    // Clear access token and ID token from local storage
    localStorage.removeItem('access_token')
    localStorage.removeItem('id_token')
    localStorage.removeItem('expires_at')
    localStorage.removeItem('user_id')
    localStorage.removeItem('user_name')
    
    this.authNotifier.emit('authChange', false)
    // navigate to the home route
    router.replace('home')
  }

  isAuthenticated () {
    // Check whether the current time is past the
    // access token's expiry time
    let expiresAt = JSON.parse(localStorage.getItem('expires_at'))
    return new Date().getTime() < expiresAt
  }

}
